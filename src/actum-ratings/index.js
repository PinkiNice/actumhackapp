import { db } from './dataset.js';
import rating from './rating.js';

export default {
  db,
  rating
}