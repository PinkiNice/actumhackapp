import { db } from './dataset.js';

function teamTaskFittingCoefficient(task, team) {
}

function getTeamStrength(project) {
  const task = project.task;
  const team = project.team;

  /* 
    How good should this team be at this task? Does it fit the task speciality? 
    Or they are trying new technologies, so we don't expect much?

    (Sum of profiled to project role HardSkills of every member) * average(SoftSkills) *
    * teamTaskFittingCoefficient(task, team)
  */

  return 1;
}

function averageTeamsStrength(projects) {
  let strengthSum = 0;

  projects.forEach(project => {
    strengthSum += getTeamStrength(project)
  });

  return strengthSum / projects.length;
}

function projectSuccessCoefficient(project) {
  const task = db.tasks.find(task => {
    return task.projects.indexOf(project) !== -1;
  });
  const team = project.team;
  const place = project.place;

  const otherProjects = task.projects;
  const averageOpponentStrength = averageTeamsStrength(otherProjects);

  const teamStrength = getTeamStrength(project);

  let competitionCoefficient = averageOpponentStrength / teamStrength;
  // This coefficient may take into account how many teams project surpassed?
  // How good were these teams? Was the hackathon really challenging? Or was it easy one?

  // Only allow increasing rating grow if weaker teams win
  // Do not reduce received rating if strong team wins
  // Simplest possible calculation
  if (competitionCoefficient > 3) {
    competitionCoefficient = 3;
  } else if (competitionCoefficient < 0.6) {
    competitionCoefficient = 0.6;
  }

  if (place == 0) {
    return competitionCoefficient = 0;
  }

  return competitionCoefficient;
}

function impliedEffortCoefficientHS(projectPartPercentage) {
  const magicCoefficientValue = 0.75;
  // Between 0.25 at zero effort, 2.25 at 100% effort
  return Math.pow(magicCoefficientValue + projectPartPercentage, 2);
}

function impliedEffortCoefficientSS(projectPartPercentage) {
  const magicCoefficientValue = 1;
  return Math.pow(magicCoefficientValue + projectPartPercentage, 1.4);
}

function roleHardSkillGain(role, participation) {
  const roleHSC = role.hsc;

  // Roles effort percentage can't be less than 10% just for fun
  let rolePercentage = participation.project.effortPartition[role.name] || 0.1;
  const roleTags = participation.tags.filter(tag => tag.tag.role == role);
  const roleTagsAmount = roleTags.length;

  const roleTagValue = participation.tags.filter(tag => tag.tag.role == role).reduce((acc, tag) => {
    return acc + tag.value;
  }, 0) / roleTagsAmount;

  //console.log(participation.tags);
  //console.log('SUM TAG VALUE', roleTagValue);
  //console.log("ROLE TOTAL TAGS AMOUNT", roleTagsAmount);

  if (rolePercentage < 0.1) {
    rolePercentage = 0.1;
  }
  //const rolePercentage = participation.rolesProjectImpact[role.name] || 0.1;

  const roleImpliedEffort = impliedEffortCoefficientHS(rolePercentage) * roleTagValue;
  let projectSuccessK = projectSuccessCoefficient(participation.project);

  if (projectSuccessK == 0 && role == db.roles.MANAGER) {
    projectSuccessK = -0.5;
  }

  const gain = roleHSC * roleImpliedEffort * projectSuccessK;

  if (participation.project.place == 0) {

  }

  return gain;
}

function hardSkillBase(user) {
  let hs = 0;
  const userParticipations = db.userParticipations(user);

  userParticipations.forEach(participation => {
    /* Get role HS coefficient, get role Implied Effort * projectSuccessCoefficient */
    const userProjectRoles = participation.roles;
    userProjectRoles.forEach(role => {
      hs += roleHardSkillGain(role, participation);
    })
  });

  return hs;
}

function hardSkillProfile(user, wantedRole) {
  let hs = 0;
  const userParticipations = db.userParticipations(user);

  userParticipations.forEach(participation => {
    const userProjectRoles = participation.roles;
    userProjectRoles.forEach(participationRole => {
      if (participationRole == wantedRole) {
        hs += roleHardSkillGain(wantedRole, participation);
      }
    })
  });

  return hs;
}

function hackathonPower(hackathon) {
  /*
    Returns hackathon power or awesomety
    Counted from amount of participants and teams, winning prize
  */
}

/* SOFT SKILL HELPERS */
function teamCommunicatingDifficulty(project) {
  const team = project.team;
  const teamSize = team.members.length;

  // The most basic counting example
  return teamSize / 3;
}

function softSkillProfile(user, wantedRole) {
  let ss = 0;
  const userParticipations = db.userParticipations(user);

  userParticipations.forEach(participation => {
    const userProjectRoles = participation.roles;
    userProjectRoles.forEach(participationRole => {
      if (participationRole == wantedRole) {
        ss += roleSoftSkillGain(wantedRole, participation) 
          * teamCommunicatingDifficulty(participation.project);
      }
    })
  });

  return ss;
}

function hardSkillProfileCommon(user, wantedRole) {
  let hs = 0;
  hs += hardSkillProfile(user, wantedRole);
  hs += hardSkillProfile(user, db.roles.COMMON);
  return hs;
}

function softSkillProfileCommon(user, wantedRole) {
  let ss = 0;
  ss += softSkillProfile(user, wantedRole);
  ss += softSkillProfile(user, db.roles.COMMON);
  return ss;
}

function roleSoftSkillGain(role, participation) {
  const roleSSC = role.ssc;
  // Soft skills are growing at different minimal step
  // The more roles you take, the bigger your soft skills considered
  // Those coefficients helps to mark that
  let rolePercentage = participation.project.effortPartition[role.name] || 0.33;

  if (participation.project.place == 0) {
    return 0.00;
  }

  if (rolePercentage < 0.33) {
    rolePercentage = 0.33;
  }

  const roleImpliedEffort = impliedEffortCoefficientSS(rolePercentage);
  const gain = roleSSC * roleImpliedEffort;
  return gain;
}

function softSkillBase (user) {
  let ss = 0;

  const userParticipations = db.userParticipations(user);

  userParticipations.forEach(participation => {
    const userProjectRoles = participation.roles;
    let participationSoftSkillGain = 0;

    userProjectRoles.forEach(role => {
      participationSoftSkillGain += roleSoftSkillGain(role, participation);
    });

    const communicationBonus = teamCommunicatingDifficulty(participation.project);
    participationSoftSkillGain *= communicationBonus;

    ss += participationSoftSkillGain;
  });

  return ss;
}

function totalRating (user) {
  const clear = Number(hardSkillBase(user) + softSkillBase(user)).toFixed(2);

  const participations = db.userParticipations(user);
  const length = participations.length;

  const mvpProjects = participations.reduce((acc, participation) => {
    if (participation.project.place !== 0) {
      return acc + 1;
    } else {
      return acc;
    }
  }, 0);

  //If percentage lower than 0.8 - than coefficient is less than 1, else - more than 1 (increasing)
  const base = 0.8;

  if (participations.length == 0) {
    return 0;
  }

  const mvpPercentage = mvpProjects / db.userParticipations(user).length;
  return Number(clear * mvpPercentage).toFixed(2);
}

function totalTeamRating (team) {
  const teamProjects = team.projects;
  const members = team.members;

  let averageTeamMemberRating = Array.from(members).reduce((acc, member) => {
    return acc + totalRating(member) / members.length;
  }, 0);

  return averageTeamMemberRating.toFixed(2);
}

export default {
  hardSkillBase,
  softSkillBase,
  softSkillProfile,
  hardSkillProfile,
  softSkillProfileCommon,
  hardSkillProfileCommon,
  totalRating,
  totalTeamRating,
}