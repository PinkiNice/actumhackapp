class Role {
  constructor({ name, hsc, ssc }) {
    if (hsc * ssc !== 1) {
      throw new Error("Hard Skill Coefficient * Soft skill Coefficient must be equal to 1");
    }

    this.name = name;
    this.hsc = hsc;
    this.ssc = ssc;
  }
}

const ROLES = {
  DESIGNER: new Role({
    name: 'designer',
    hsc: 1.2,
    ssc: 1 / 1.2,
  }),
  PROGRAMMER: new Role({
    name: 'programmer',
    hsc: 2,
    ssc: 1 / 2,
  }),
  MANAGER: new Role({
    name: 'manager',
    hsc: 0.5,
    ssc: 1 / 0.5,
  }),
  COMMON: new Role({
    name: 'common',
    hsc: 1,
    ssc: 1,
  }),
};

const ROLES_ARRAY = [ROLES.DESIGNER, ROLES.PROGRAMMER, ROLES.MANAGER, ROLES.COMMON];

class Tag {
  constructor({ name, role }) {
    if (ROLES_ARRAY.indexOf(role) === -1) {
      throw new Error("Attempt to create tag with undefined role");
    }

    this.name = name;
    this.role= role;
  }
}

class Task {
  constructor({ projects, description, specialization }) {
    this.projects = projects;
    this.description = description;
    this.specialization = specialization;
  }
}

class Hackathon {
  constructor({ tasks }) {
    this.tasks = tasks;
  }
}

class Project {

  /*
    @Param: team - object describing team which made this concrete project.
      It is immutable and has primary key to team to identify it later in time, but
      members inside object are taken once and can not be changed after project is over.

    @Param: source - some information about code itself 
      (for percentage of used programming languages for ex.)

    @Param: place - what place project took on hackathon in it's nomination

    @Param: participations - array of all players, who are authors of these project. 
      (Because teams may change in time)

    @Param: tags - related to project skills (Java, Machine Learning, Web-Design)
      Those skills will be approved for participations

    @Param: task - description of the task, may contain some biases about rating grows.
      for example - gained ratings for designer in Blockchain task is reduced.
  */
  constructor({ team, tags, roles, source, place, task, brainstormMap, effortPartition }) {
    this.team = team;
    this.team.projects.push(this); 
    this.source = source || "Source is not attached to the project.";
    this.place = place;

    // Optionally submitted by team on 1st check-point
    this.brainstormMap = brainstormMap || null;

    // Filled by experts after project submission as MVP

    /*
      [{role: ROLES.DESIGNER, effort: 20%}, {role: ROLES.PROGRAMMIST, effort: 40%}]
    */
    this.effortPartition = effortPartition;

    // Roles used in this project - they can be sucked out from tags

    // Filled by team members at brainstorm stage (or later during development)
    this.tags = tags || [];
    this.roles = roles || [];
    // Freezed in time list of members who participated in project
    this.participants = Array.from(team.members);
  }
}

class Participation {
  /*
   @Param: hackathon: reference some particular hack taked place some time ago.
   @Param: project: references project which has been submitted to jury by the team user was in.
   @Param: tags: project required skills in which this person participated for ex. ["Java", "Back-end"]
   @Param: team: under what 'label' Participation taked place in hackathon
  */
  constructor({ user, project, roles, tags }) {
    this.user = user;
    this.project = project;
    this.roles = roles || [];
    /*
      { tag: 'java', value: 0.3, role: ROLE.PROGRAMMER }, { tag: 'UI/UX', value: 1, role: ROLE.DESIGNER }
      Sum of values of each tag must be equal to 1
      (the more of the tag you take the less you leave for your teammates)
    */
    this.tags = tags || [];
  }
}

class User {
  /*
    User info abstracted from hackathon participating.
  */
  constructor({ username, common, socials, roles }) {
    this.username = username;
    this.common = common || {};
    this.socials = socials || {};
    // This is not used in rating, but just to help user find team
    this.roles = roles || [];
  }
}

class Team {
  constructor({ name, members, projects }) {
    this.name = name;
    this.members = members || [];
    this.projects = projects || [];
  }
}

/*module.exports = {
  Role,
  User, 
  Participation,
  Hackathon,
  Project,
  Team,
}*/

const USERS = [
  new User({ username: '3D Waffle'}),
  new User({ username: 'Hightower'}),
  new User({ username: 'Papa Smurf'}),
  new User({ username: '57 Pixels'}),
  new User({ username: 'Hog Butcher'}),
  new User({ username: 'Pepper Legs'}),
  new User({ username: 'rare'}),
  new User({ username: 'Houston'}),
  new User({ username: 'Pinball Wizard'}),
  new User({ username: 'Accidental Genius'}),
  new User({ username: 'Hyper'}),
  new User({ username: 'Pluto'}),
  new User({ username: 'Alpha'}),
  new User({ username: 'Jester'}),
  new User({ username: 'Pogue'}),
  new User({ username: 'Airport Hobo'}),
  new User({ username: 'Jigsaw'}),
  new User({ username: 'Prometheus'}),
  new User({ username: 'Bearded Angler'}),
  new User({ username: 'Jokers Grin'}),
  new User({ username: 'Psycho Thinker'}),
  new User({ username: 'Beetle King'}),
  new User({ username: 'Judge'}),
  new User({ username: 'Pusher'}),
  new User({ username: 'Bitmap'})
];

const TEAMS = [
  new Team({name: 'name0', members: USERS.slice(0, 3)}),
  new Team({name: 'name1', members: USERS.slice(3, 5)}),
  new Team({name: 'name2', members: USERS.slice(5, 7)}),
  new Team({name: 'name3', members: USERS.slice(7, 10)}),

  new Team({name: 'name4', members: USERS.slice(10, 15)}),
  new Team({name: 'name5', members: USERS.slice(15, 19)}),
  new Team({name: 'name6', members: USERS.slice(19, 21)}),
  new Team({name: 'name7', members: USERS.slice(21, 23)}),
  new Team({name: 'name8', members: USERS.slice(23, 26)}),

  new Team({name: 'name9', members: USERS.slice(0, 4)}),
  new Team({name: 'name10', members: USERS.slice(4, 5)}),
]

const TAGS = [
  new Tag({name:'Blockchain', role: ROLES.PROGRAMMER}),
  new Tag({name:'UX', role: ROLES.DESIGNER}),
  new Tag({name:'Problem analysis', role: ROLES.MANAGER}),
  new Tag({name:'Idea generation', role: ROLES.COMMON}),
  new Tag({name:'Cryptocurrency', role: ROLES.PROGRAMMER}),
  new Tag({name:'Presentation making', role: ROLES.DESIGNER}),
  new Tag({name:'Team managment', role: ROLES.MANAGER}),
  new Tag({name:'Team spirit', role: ROLES.COMMON})
];

function randomProjectTags() {
  const tagsNumber = Math.ceil(Math.random() * 3) + 2;
  const startIndex = Math.floor(Math.random() * (TAGS.length - 1));
  const dublicated = Array.from(TAGS.concat(TAGS));
  return dublicated.slice(startIndex, startIndex + tagsNumber);
}

function ejectRolesFromTags(project) {
  // Add 'Common' role every time

  const array = Array.from(project.tags);

  const roles = array.reduce((acc, tag) => {
    if (acc.indexOf(tag.role) === -1) {
      acc.push(tag.role);
    }

    return acc;
  }, []);

  array.push(ROLES.COMMON);
  return roles;
}

function randomEffortPartition(project) {
  const roles = project.roles;
  let randoms = [];
  let sum = 0;

  for (let i = 0; i < roles.length; ++i) {
    const random = Math.random();
    sum += random;
    randoms.push(random);
  }

  let normalized = randoms.map(random => random / sum);
  const result = {}
  roles.forEach((role, i) => {
    result[role.name] = normalized[i];
  });
  return result;
}

const PROJECTS = [
  new Project({team: TEAMS[0], place: 1, }),
  new Project({team: TEAMS[1], place: 2, }),
  new Project({team: TEAMS[2], place: 3, }),
  new Project({team: TEAMS[3], place: 0, }),

  new Project({team: TEAMS[0], place: 0, }),
  new Project({team: TEAMS[1], place: 3, }),
  new Project({team: TEAMS[4], place: 1, }),
  new Project({team: TEAMS[5], place: 2, }),

  new Project({team: TEAMS[9], place: 3, }),
  new Project({team: TEAMS[10],place: 1, }),
  new Project({team: TEAMS[6], place: 2, })
];

PROJECTS.forEach(project => {
  project.tags = randomProjectTags();
  project.roles = ejectRolesFromTags(project);
  project.effortPartition = randomEffortPartition(project);
});

const TASKS = [
  new Task({description: "Task 1", projects: PROJECTS.slice(5,9)}),
  new Task({description: "Task 2", projects: PROJECTS.slice(9,12)}),
  new Task({description: "Task 3", projects: PROJECTS.slice(0,5)}),
]

const HACKATHONS = [
  new Hackathon({tasks: TASKS[0]}),
  new Hackathon({tasks: TASKS[1]}),
  new Hackathon({tasks: TASKS[2]})
]

const PARTICIPATIONS = [];

function getRandomElement(array) {
  const randomIndex = Math.round(Math.random() * (array.length - 1))
  return array[randomIndex];
}

function addElementIfNotAlreadyAdded(array, element) {
  if (array.indexOf(element) === -1) {
    array.push(element);
  }
}

const mockUserSelectedRoles = (project) => {
  /*
    Some of roles may not be assigned during this procedure
    which leads to tags in the project which can not be assigned to anyone
  */
  const random = Math.round(Math.random() * 10);
  const selected = [];

  if (random >= 5) {
    addElementIfNotAlreadyAdded(selected, getRandomElement(project.roles));
  } 
  if (random >= 1) {
    addElementIfNotAlreadyAdded(selected, getRandomElement(project.roles));
  } 
  if (random == 0) {
    addElementIfNotAlreadyAdded(selected, getRandomElement(project.roles));
  }

  return selected;
}

function notTakenRoles(project, members) {
  const lonlyRoles = Array.from(project.roles);

  members.forEach(member => {
    member.roles.forEach(role => {
      // if member role yet haven been deleted from lonly list
      if (lonlyRoles.indexOf(role) !== -1) {
        // delete role from lonly list
        lonlyRoles.splice(lonlyRoles.indexOf(role), 1);
      }
    })
  })

  return lonlyRoles;
}

function populateMembersWithTags(project) {
  const projectTags = Array.from(project.tags);

  const projectRoles = project.roles;

  const team = project.participants.map(user => {
    const userRoles = mockUserSelectedRoles(project);
    //console.log("GENERATED USER ROLES", userRoles);
    //console.log("FOR THIS TAGS", project.tags);
    return new Participation({ user, project, roles: userRoles })
  });

  const lonlyRoles = notTakenRoles(project, team);

  if (lonlyRoles.length !== 0) {
    //console.log(lonlyRoles);
    /* Assign everything left to the person with least amount of roles */
    let leastRolesMember = team[0];

    team.forEach(member => {
      if (member.roles.length < leastRolesMember.roles.length) {
        leastRolesMember = member;
      }
    });

    leastRolesMember.roles = leastRolesMember.roles.concat(lonlyRoles);
    //console.log("THESE ROLES LEFT UNCHOSEN", lonlyRoles);
    //console.log("NOW THEY HAVE HOME!", leastRolesMember.roles);
    //throw new Error("WOW! Some roles in the project hasn't been assigned to anyone!!! It can't be!");
  }

  let personIndex = -1;

  const teamRoles = [];

  /* Generate helping data structure which will help us to divide tags*/
  team.forEach(member => {
    member.roles.forEach(role => {
      teamRoles.push({ member, role });
    })
  })

  const tagRepentance = {};

  function increaseTagRepentance(tag) {
    if (tagRepentance[tag]) {
      tagRepentance[tag]++;
    } else {
      tagRepentance[tag] = 1;
    }
  }

  function giveTagToMember(member, tag) {

    // If there is already a such tag - increase value by 1
    const foundTag = member.tags.find(satTag => satTag.tag == tag);
    if (foundTag) {
      foundTag.value += 1;
    } else {
      member.tags.push({ tag, value: 1});
    }
  }

  /* For each member add 1 random tag from his role */
  teamRoles.forEach(teamRole => {
    const member = teamRole.member;

    const filteredTags = projectTags.filter(tag => { 
      //console.log("TEAM ROLE", teamRole);
      //console.log(tag.role.name, teamRole.role.name);
      return tag.role == teamRole.role
    });

    if (filteredTags.length == 0) {
      throw new Error('No tags left after filtering on initial tag giveaway. (That should not happen)');
    }
    const randomTag = getRandomElement(filteredTags);

    increaseTagRepentance(randomTag);
    giveTagToMember(member, randomTag);
  });

  /* sharing tags between members */
  let i = 0;
  while (projectTags.length !== 0) {
    // Protection from overlow. Make 'i' to cycle over array
    i = i % teamRoles.length;
    const teamRole = teamRoles[i];

    //console.log("SEARCHING TAGS FOR", teamRole.role.name);
    //console.log(projectTags);
    const filteredTags = projectTags.filter(tag => tag.role == teamRole.role);

    if (filteredTags.length > 0) {

      const randomTag = getRandomElement(filteredTags);

      if (Math.random() > 0.5) {
        increaseTagRepentance(randomTag);
        giveTagToMember(teamRole.member, randomTag);
      }

      // Delete this tag from counted tags with 50% chance
      if (Math.random() > 0.5) {
        projectTags.splice(projectTags.indexOf(randomTag), 1);
      }
      
    }


    i++;
  }

  // Now divide every tag value 
  team.forEach(member => {
    member.tags.forEach(tag => {
      tag.value = tag.value / tagRepentance[tag.tag];
    })
    PARTICIPATIONS.push(member);
  });
}


PROJECTS.forEach(project => {
  populateMembersWithTags(project);
});

function userParticipations(user) {
  return PARTICIPATIONS.filter(part => {
    return part.user == user;
  });
}

function teamMvps(team) {
  const teamProjects = Array.from(team.projects);

  return teamProjects.reduce((acc, proj) => {
    if (proj.place && proj.place > 0) {
      return acc + 1;
    }
    return acc;
  }, 0);
}

function userMvps(user) {

  const userProjects = PARTICIPATIONS.filter(participation => {
    return participation.user == user && participation.project.place && participation.project.place > 0
  });

  return userProjects.length;
}

export const db = {
  roles: ROLES,
  users: USERS,
  participations: PARTICIPATIONS,
  projects: PROJECTS,
  hackathons: HACKATHONS,
  teams: TEAMS,
  tasks: TASKS,
  userParticipations,
  teamMvps,
  userMvps,
};

