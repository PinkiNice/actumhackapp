import Vue from 'vue'
import Router from 'vue-router'
import RatingTable from '@/components/RatingTable';
import HrPanel from '@/components/HrPanel';

Vue.use(Router)

export default new Router({
  routes: [
/*    {
      path: '/',
      name: 'RatingTable',
      component: RatingTable
    },*/
    {
      path: '/',
      name: 'HrPanel',
      component: HrPanel,
    }
  ]
})
